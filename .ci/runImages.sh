#!/bin/bash
sudo docker stop exercise || true
sudo docker rm exercise || true
sudo docker rmi -f $(docker images -q -f dangling=true) || true
sudo docker run -d -p 80:80 --name exercise imamdnl/exercise